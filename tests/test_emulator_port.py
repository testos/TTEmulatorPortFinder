# ecoding: utf-8

from src.utils import ttutil
import emulator_port
import json
import unittest
import os


class Test_emulator_port(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(Test_emulator_port, cls).setUpClass()
        emulator_port.read_config()

    @classmethod
    def tearDownClass(cls):
        super(Test_emulator_port, cls).tearDownClass()

    def test_tbox(self):
        emulator = emulator_port._EMULATORS['mumu']
        ps = emulator_port.get_processes(emulator)
        ports = emulator_port.get_ports(emulator)
        if (len(ps) == 0):
            print("please start at least one '{}'!".format(emulator.name))
        self.assertNotEqual(len(ps), 0)
        self.assertEqual(len(ps), len(ports))
        ttutil.check_adb_connectable_by_ports(ports)

    def test_write_config(self):
        config = {
            "emulator_ip":
            "127.0.0.1",
            "emulators": [{
                "type": "tbox",
                "name": "腾讯手游助手",
                "process_name": ":TBoxHeadless.exe",
                "default_port": 5555,
                "re_port": "^5\\d{3,6}"
            }, {
                "type": "nox",
                "name": "夜神模拟器",
                "process_name": ":NoxVMHandle.exe",
                "default_port": 62001,
                "re_port": ""
            }]
        }
        test_file = 'test_config.json'
        emulator_port.write_config(config, dest=test_file)
        with open(test_file, 'r', encoding='utf-8') as f:
            new_config = json.load(f)
            self.assertEqual(new_config['emulators'][1]['type'], 'nox')
        os.remove(test_file)
